import React, { useEffect } from 'react'
import useGlobal from '../clockStore'
import HotelClock from './HotelClock.js'
import '../styles/App.css'

function App() {
  const [globalState, globalActions] = useGlobal()

  useEffect(() => {
    const appTimer = setInterval(() => {
      globalActions.updateDate()
    }, 1000)

    return (() => { clearInterval(appTimer) })
  })

  return (
    <div className="App" id="clockApp">
      <HotelClock />
      <HotelClock timeOffset="5"/>
      <HotelClock timeOffset="-3"/>
    </div>
  )
}

export default App