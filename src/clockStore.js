import React from 'react'
import useGlobalHook from 'use-global-hook'
import Moment from 'moment'

const initialState = {
  newDate: Moment().toDate()
}

const actions = {
  updateDate: (store) => {
    const newDate = Moment().toDate()
    store.setState({ newDate })
  }
}

const useGlobal = useGlobalHook(React, initialState, actions)

export default useGlobal